# Schluss Service provider

This project contains an implementation example for Schluss service providers (data providing and data requesting organizations). Before we get started, let us first explain some of the definitions we use:

### Service provider

A Schluss Service provider, as the name already states, is an entity (individual or organization) 
which provides services that have an interaction with the Schluss users' app. 
The type of interaction is, for now, to provide data to or request data from the Schluss user. A service provider can also cover both roles.
In the near future we also see possibilities for other types of interactions and services 
like Digital Assistents, AI, data crunching, calculations and so on.

### Data provider

A Data provider is a data providing entity (individual or organization). The providable data is configured in the Data providers' config.json file. Data can be provided in many ways and many forms, therefore a Data provider will have to create a plugin for use inside the Schluss app that will handle the provided data. Having a plugin also gives the Data provider freedom on how to identify and authorize the user in it's own backend system or web portal.

An example of how a data provider could work is shown here: [dataprovider/](./dataprovider/)

### Data requester

A Data requester is a data requesting entity (individual or organization). All requested data is configured inside the Data requesters' config.json file.

An example of how a data requester could work is shown here: [datarequester/](./datarequester/)

### Data request
A Data request is always initiated from a Data requester. A data request contains all information needed for the Schluss app to handle the routes and plugins offered by the Data providers.

### How everything connects

As of this point a self created standard with config.json files is used to 
connect Schluss Service providers (data providers and data requesters) to each
other via the Schluss app. Both Data providers and Data requestes publishe a config.json, that must be publically available.

In the near future this will probably become a form of JSON-LD (https://json-ld.org/).

Example structure of json.config files will be added here:

`todo: config.json file structure`