# Data requester

This document describes what is needed to be able to do data requests. These are:
- Create an API to recieve data and status updates
- Define the data to be requested
- Integrate the QR code

In the end you will be able to display a QR code, containing all the information needed for a data request, to your customer. After your customer scans the QR code within the Schlus app the requested data is personally collected. And finally the data is safely sent to your API. Sounds easy, right? Ok, let's dive right in then.

## Requirements

### Have a Key Pair:   
In order for all this to work, you need to have a Public/Private keypair. You need [GnuPG](https://www.gnupg.org/download/index.html) to generate a key. It is done like this:

**Generate Key Pair**:   
```
> gpg --full-gen-key

Select the kind of key you want:
Choose (1) RSA and RSA (default)
> 1

Then enter the requested keysize:
> 4096

Specify how long the key should be valid:
> 0

Enter a user ID to identify the key:
Under Real name, enter your key identifying name
> your-chosen-user-id-here

E-mail address:
> your@email-address.com

Confirm all settings:
Choose (O)kay
> O

Choose and confirm a password to protect your private key:
> a-secure-password

The key will now be generated and stored. In the end you will get a message like:
public and secret key created and signed.
```

**Export public key**
```
> gpg --armor --export your-chosen-user-id-here > /path/to/pubkey.pem
```

**Export private key (if needed)**
```
> gpg --armor --export-secret-keys your-chosen-user-id-here > /path/to/private.pem
```
---



## 1. Create an API to recieve data and status updates
You need at least one publically available endpoint where the Schluss app can send the requested data to. Optionally you can expose and endpoint to recieve status updates, so you can monitor the progress of the data request. Also status updates can be used to enhance the user experience by showing an on-screen progress indicator.

### /contracts
API endpoint where POST requests containing the requested data in the form of a `contract` will be recieved, directly from the Schluss app. Note the `[token]` will contain a unique session identifier, generated yourself and put into the QR code at the start of a data request. 

```
[POST] /contracts
Content-type: application/json
Header : Authorization [token]
Body : {
 "contract":"-----BEGIN PGP MESSAGE .... END PGP MESSAGE-----",
 "signingKey":"-----BEGIN PGP PUBLIC KEY .... END PGP PUBLIC KEY-----"
}

Return HTTP-STATUS 200 - when operation succeed
Return any other HTTP-STATUS when operation did not succeed
```

`contract`: A PGP encrypted message containing the requested data. Use your private key to decrypt the message


### /status (optional)
API endpoint where POST requests containing status updates will be recieved, directly from the Schluss app. Note the `[token]` will contain a unique session identifier, generated yourself and put into the QR code at the start of a data request. 

```
[POST] /status
Header: Authorization [token]
Content-type: application/json
Body: {
  "state" : "connecting"
}

Return HTTP-STATUS 200 - when operation succeed
Return any other HTTP-STATUS when operation did not succeed
```

`state`: 
- "connecting": When the user scans the QR code inside the Schluss, the app validates and verifies the data request. After this the 'connecting' state is sent. When recieving this state, you could for example hide the QR code and display a message on screen 'telling the user to follow the instructions inside the Schluss app'.   
- "done": Right after the user finishes the datarequest and the data is sucessfully sent to /contracts, the app also sends an additional status update 'done'.

## 2. Define the data to be requested
A data request is defined using a config.json file. Although this is not well documented for now, some examples can be found here: [Schluss services directory](https://services.schluss.app/). Note that the config.json file, the organizations' public key and the api to be able to recieve status updates and data need to be publicly available.

A config.json file contains:   
- information about the data requesting organization
- url to where the public key of the data requesting organization can be found
- an `import` section where one or more possible data requests are defined

An `import` section contains:   
- a scope; which will act as the unique identifier for a data request
- information about the specific data request
- `endpoints`; where the status updates and the requested data will be sent from the users' Schluss app
- `providers`; where the different data providing organizations are described

An `endpoints` section contains:   
- status; where status updates will be sent to
- return; where the data contract will be sent to

A `providers` section item contains:  
- the required plugin by the Schluss app
- information about what sort of data will be requested from that dataprovider
- src; where the dataprovider hosts it's config.json file
- `fields`; the actual attributes to be requested from that dataprovider

A `fields` section item within a `providers` section item contains:   
- match; must match with the attribute name in the dataprovider config.json file
- required; whether is this attribute mandatory or optional

## 3. Integrate the QR code
To be able for Schluss app users to recieve and process your data request, you need to generate and display a QR code. This QR code will be scanned using the Schluss app and contains all the information needed to guide the user when retrieving the right data from the right sources. Also the QR code contains additional information, like a session token so you can link all recieved messages that same person who opened the datarequest.  

### QR Code URL format
Simply put, the QR code is a visual representation of an URL with the following format:

`https://schluss.org/redirect/connect/[token]/[configurl]/[scope]`

Format explained:
- `https://schluss.org/redirect/connect/`:   
Every data request url must begin with this url. It tells the Schluss app what to expect.
- `token`:   
Generated by yourself, an unique session token. This will be used by the Schluss app as in the form of an Authorization header when accessing your API endpoints. There is no restriction on the used datatype. Although we prefer an unique UUID V4.
- `configurl`:    
a publicly available url to where your config.json is reachable by the Schluss app
- `scope`:   
since your config.json can contain multiple different data requests, the scope defines which one

Note: all the url parameters are mandatory and expected in the same order as above. The values in all parameters could contain non-standard characters, potentially breaking the normal functionality of an URL, therefore each parameter needs to be [escaped](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent).

Of course we want to make integration as easy as possible, that's why we supply two ways of integrating a QR code, where handle the generation of it

### Option 1: Let Schluss generate your QR code
This option is most easy to integrate: just add an image tag like so:

```
<img src="https://qr.schluss.app/[apikey]/[token]/[configurl]/[scope]" />
```

We will do the generation for you, the returned image will be a ready to use QR code. Please contact Schluss to provide you with an `apikey` to be able to use this method.  

### Option 2: Generate the QR code with the provided Schluss library
We created a javascript library which you can integrate in your current (web)application. For more information about the implementation, see [here](https://gitlab.com/schluss/schluss-connect)

